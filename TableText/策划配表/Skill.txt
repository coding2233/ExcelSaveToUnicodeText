#	技能配置表																	
#	Id		Category	SkillName	SkillTip	SkillImage	TargetType	TargetCount	SkillDamage	Frequency	CastTime	Cooldown	CastRange	ManaCosts	Animation	EffectAsset	TargetBuffId	Trigger
#	int		int	string	string	string	int	int	int	float	float	float	int	int	string	string	int	string
#	技能编号	策划备注	技能类别(瞬发0、持续1)	技能名称	技能提示	技能图标(资源路径)	"目标类型(敌人0,队友1，自己2)"	目标数量(不限-1）	技能伤害	攻击频率	施法时间	冷却时间	施法范围	法力消耗值	播放动画	特效资源	目标buffId	技能触发
	1	冰天雪地	0	Skill.Iceeeeee	Default	Default	0	-1	10	0.5	1	5	10	8	Idle	Default	0	A
	2	渣渣技能	1	Skill.test02	Default	Default	0	-1	10	0.5	3	5	10	8	Idle	Default	0	AA
	3	渣渣技能	1	Skill.test03	Default	Default	0	-1	2	0.5	3	2	9	7	Idle	Default	0	B
