﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace TableText
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                DealExcel(args);
            }
            else
            {
                //test
                //args = new string[] { @"..\..\策划配表",@"..\..\Assets\GameSoul\DataTables" };
                //DealExcel(args);
                Console.WriteLine("程序参数错误!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }

        private static void DealExcel(string[] args)
        {
            string excelFPath = Path.GetFullPath(args[0]);
            string saveFPath = Path.GetFullPath(args[1]);
            if (Directory.Exists(excelFPath))
            {
                if (!Directory.Exists(saveFPath))
                    Directory.CreateDirectory(saveFPath);

                string[] allExcelPaths = GetAllExcePaths(excelFPath);
                foreach (var item in allExcelPaths)
                {
                    SaveAs(item, saveFPath, excelFPath);
                }
            }
        }

        private static string[] GetAllExcePaths(string foldePath)
        {
            List<string> allExcelPaths = new List<string>();
            DirectoryInfo dirInfo = new DirectoryInfo(foldePath);
            FileInfo[] filesInfo = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
            Console.WriteLine("文件总数:" + filesInfo.Length);
            foreach (var item in filesInfo)
            {
                if (item.Extension == ".xls" || item.Extension == ".xlsx")
                {
                    allExcelPaths.Add(item.FullName);
                    Console.WriteLine("获取excle文件:" + item.FullName);
                }
            }
            return allExcelPaths.ToArray();
        }
        
        private static string GetTextPath(string saveAsFolder,string excelPath,string excelFolder)
        {
            string txtPath = excelPath.Replace(excelFolder, saveAsFolder);
            
            int index = txtPath.LastIndexOf("\\");
            string folderPath = (txtPath.Substring(0, index));
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
            index = txtPath.LastIndexOf(".");
            txtPath = txtPath.Substring(0, index)+".txt";
            return txtPath;
        }

        private static void SaveAs(string excelPath,string saveAsFolder,string exceFolder)
        {
            string txtPath = GetTextPath(saveAsFolder,excelPath, exceFolder);
            if (string.IsNullOrEmpty(txtPath))
                return;
            if (File.Exists(txtPath))
                File.Delete(txtPath);
            Application excelApp = new Application();
            excelApp.Workbooks.Open(excelPath);
            excelApp.ActiveWorkbook.SaveAs(txtPath, XlFileFormat.xlUnicodeText);
            excelApp.ActiveWorkbook.Close();
            excelApp.Quit();
            Console.WriteLine("成功另存:" + txtPath);
        }



    }
}
